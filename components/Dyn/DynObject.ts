import { DynObjectInput } from './types/DynObjectInput'
import { DynObjectAction } from './types/DynObjectAction'
import { DynObjectRow } from './types/DynObjectRow'

export class DynObject {
  inputs?: DynObjectInput[]
  actions?: DynObjectAction[]

  rows?: DynObjectRow[]

  elementcount?: number

  constructor(obj: DynObject) {
    Object.assign(this, obj)
  }
}
