export interface DynObjectInput {
  key?: number // not needed, only for vue

  name: string
  type: string

  displayName: string
  hint: string
  rules: any

  visible: boolean // show it in tables ?
  valueEditOnCreate?: boolean // Can be changed on create, not on edit
  valueMandatory: boolean // this value is needed
  valueHidden: boolean // dont show the values
  valueReadOnly: boolean // you can not write to it :)
  valueFixed: any | undefined // value can not be changed

  options: InputOption[]
}

export interface InputOption {
  value: string
  displayName: string
}
