export interface DynObjectAction {
  // Interface ( JSON )
  name: string;
  command: string;
  displayName?: string;
  icon?: string;
  position?: "start" | "mid" | "end";

  // Java-Script / UI only
  /* eslint-disable  @typescript-eslint/no-explicit-any */
  onCommand?: (command: string, payload?: any) => Promise<boolean>;
}
