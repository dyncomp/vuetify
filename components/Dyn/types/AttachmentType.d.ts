export interface AttachmentType {
  attachment: File;
  filename: string;
}
