export interface INavigation {
  id: string;
  displayText: string;
  icon: string;
  url?: string;
  roles: string[];
  children?: INavigation[];
}
