import { DynObjectInput } from './types/DynObjectInput'
import { AttachmentType } from './types/AttachmentType'

export function AttachmentsExtract(
  inputs: DynObjectInput[] | undefined,
  values: any
): AttachmentType[] {
  var attachments: AttachmentType[] = []

  if (inputs === undefined) return attachments

  // iterate all inputs and look for a 'file' type
  // to remember the File-Object
  inputs?.forEach((input: DynObjectInput) => {
    if (input.type === 'file') {
      // Check if the user give an file
      if (values[input.name] !== undefined) {
        attachments.push({
          attachment: values[input.name],
          filename: input.name,
        } as AttachmentType)
      }

      // remove the file from object
      values[input.name] = undefined
    }
  })

  return attachments
}
